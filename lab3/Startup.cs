using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using lab3.ViewModels;
using laba3;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace lab3
{
    public class Startup
    {
        #region Constructors

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #endregion Constructors

        #region Properties

        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Methods

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
        }

        public (List<UserCallsVm> userCallsVm, List<UserInfoVm> userInfoVm) GetData()
        {
            List<UserCallsVm> UserCalls = new List<UserCallsVm>();
            List<UserInfoVm> UserInfoVms = new List<UserInfoVm>();
            using (var db = new DatabaseContext())
            {
                var usersInfoList = db.Persons.Select(user =>
                    new
                    {
                        user.id,
                        user.first_name,
                        user.second_name,
                        user.last_name,
                        user.phone_number,
                        user.address.city,
                        user.address.street,
                        user.address.house_number,
                        cityCount = user.calls.Select(x => x.city)
                            .Distinct()
                            .Count()
                    });

                foreach (var user in usersInfoList)
                {
                    var vm = new UserInfoVm()
                    {
                        CityCount = user.cityCount,
                        City = user.city,
                        HouseNumber = user.house_number,
                        PhoneNumber = user.phone_number,
                        FirstName = user.first_name,
                        SecondName = user.second_name,
                        Id = user.id,
                        LastName = user.last_name,
                        Street = user.street
                    };


                    UserInfoVms.Add(vm);
                }

                var test = db.Persons.Select(user =>
                    new
                    {
                        user.last_name,
                        user.first_name,
                        citys = user.calls
                            .Select(call =>
                                call.city)
                            .Select(city => new UserCity() {City = city.city, Code = city.code})
                    }
                );
                foreach (var user in test)
                {
                    var vm = new UserCallsVm()
                    {
                        FirstName = user.first_name,
                        LastName = user.last_name,
                        Citys = user.citys,
                    };
                    UserCalls.Add(vm);
                }

            }

            return (UserCalls, UserInfoVms);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    string image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABtbSURBVHhe7Z0JmBxVtcc7kOmeCQiijycuz43n4+EumGSme8g8RWLIdPckkgEFNJJ09RAiIgrIIs4jme6egEFZXEAQRUASwJDpJQtgRIJIQFFf2CQaiYpARBZli5C8/6k+3X2ruqq7Jvsk/9/33a+77znn3qpb99ytblWHCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEII2eno7V24Z3cmf1giW5ybzBZXJ7OFEsI5CONVhZDdl3g2fzGcYWMim/9yMle8Ap/nJhGXyBY2JHP5qapGyO6H9BpwhEXoOb7Yk81fiN/nJzKFo5O5wk+Tg6VpcJhVyUzeUnVCRia9/Sv27skUj41jaIQK/one/oVhFfmSmLv4Heg5noVT3BkfHOqEU/wmkSv+DHG3JXKF66X3gOM8APlDakLIyEPmCugF1sIxvteTLZyG75cj3J+4YNk7VMWTZG5JbzJb+prYI9wFm8cSmfwAHOJKfH8Cnz/uyS15O76vSGZK71YzQkYO0nOgcq+Bc8xHD/DItExpf4nH9270BqtCoU2jbEUPxJHEBvYr0GPcg89F6IE+nTzPdrg1Em/LsoV1MgRTM0JGDj3Z4jGowJfGB4YSk/pL+2i0DSbbhWSu9CH9WUcyUzhTJuRTB5a8EWl8A/ONP8MhliPuWjjMy+hNZonDIW4oMVhqVzNCRg6ovOegci/tzuTfK8MgR8gVvi/zEVWtY8rc0oEYRmGOkb8PjoYJev5O/H5FApzn+3CYm/D9J8jj0en9K1rVjJCRAypvPxxhFSrzQneA4zwEWa+qeiI63dn8WdCbAGe4HZ+P6+cjicziN+B7ATo/UHVCRhbiIIlsaTqGUqdqVBVU7vObOog4Qa74CzjBn5JzCx9Dehth94AMv+BkjyD+RrmRqOqEjCwqDqI/HXg5iAzFoP/lnoHCBzUq1NO/6LXQux5p/QFOsQkO8yoc427Y57o4tCIjmSBDrHj/0BhxIlR4mWPcibjTYXeb9Bzx3FDqiAuW7TVt/tLXJTLFMxB3r92bZJck2XOQEY/dg2SKZ2MyPtM9SUdFvxYO8TN8rsHnhVMG8+OgdzK+yxzj9MRAqR2ONJjIFR6GjiznPo34nyLN1XCUxUFuNhKyU2M7iMccRJZoUfFXJbP54+LZ0thktvgdVP5HoDdHhleQny2OEc8Wr5qSHeqI54pHJbL52+E8c6E3R+6HaFKEjFzKDlLMxTOlLjMg7oHknJsP0t7ip+g5jsZ8YjQ+r0GcfZe8d+HCPeFASTjKMqSzUibs4kQ9mUKf3B/RLAgZuYiDoFI75iAYHt2H4dKPEH+69BSqalPWldWq4mqNsklmirNhewrsLrUdKFf8G3qRk1RMyMjEa4iFCl7oyQwdjgr/4ERMvjXaBhV/ADYrIPu6RtlMm7+gDc7xa3v4lS3+BQ50h0zuVUzIyKTiIPoz9Ilc4Z2o3L9GZf+U3Rvkiv8rvQW+l1TFgfQySONGfA4ms6WMbG0XB4L+WlUhZOQiDoLK/aDdK9g9A77be6qKf+2ek3+XDLnic4c60St4VnhxhKnzhg6RXmfqhfaerCclDr9fSV92b4uqETIyEQdBZf6cTLZlY2EloJLbz3BgHnGa9gjX2gYuYHdFWV60J+XQX4te5AI42uPofa6W50RsRUJGImUHqV/mNSfh9p1y1z0SM0zsX1Cdp8BBnk7kCtfEs0MTZaMjHGsIeaxODBaPUBVCRg7qIJeixe81A1r/daoSkmVfcRgMt34O2Up8bkLFvxvhDulpegbyH1dVGXI92JMrJaB/c/kGYvFchLMQzlcVQkYO4iCozLeiYl/qCk+pio2tJ44ju3NzhVfxefvkgUK3u+KjB7F7HnGi5Lzlb4KjySO4j8Zzpa/aCoSMJLQHaTjEEtSRfgjnKOLzD3Cga+EE1/k7SHmTo9xpx3BrrHwnZMRRcRD9WcXTQXLF5+UhKXGQqQM3yYqVvLDB00Eq0EHIiEZe0oCKL8u6q52h8JL5Gz3Hk4j7Z/l78WX7M1t4DrbrTb06u2zx6URm8fs1O0IIIYQQQgghhBBCCNndiUStA1ui6WMisb7/0ShCdk/27Zy1XzhmHdkaTfcjlOAc61tj6U12QJyqbV26+kdHOqyTkNdFkcNOrO6xIrsIbR3W2LbOdG/QEI6lP9HabnW1dM46NHRoeqd6Ai4ctU5tjVkbqk5hhm3kIOIYZj7hjlRcRWRXABXqKvMCDzNsRMV7GK32xW2x1M7xsuV394ZRaT/SGrV+5zjWbecg68x8wtHUlSoiuwSHpvdtiaY+GIlZM9wXWwLicpFODCE0wBlOiUTTWRnCoBK+7NS3Cm3jUm/RlHcokWjKchzbNnIQ9Fq3OvOxzlAR2dWwW17zYiOoyJO9xs98AypEwdRHGo+1xFLvUZUdxuho+sPmcW2zHqR95n/inG+PxNJPIVwd6prOV4fusvT27ukew6vEH1QI2PyfaYPKuFriVWOH0Nb+mTe7jmnbTNLJ7oX0AGbF0uiGwCZl2tghmj5exTuGrpP2dh0PHYRsOegN1poVS6MbIsMM00YC5ikLVbxjODQ9xnFMdBCyNdgcBwmNO24f00ZCJJa6U6U7BjoI2RZsloMcmt7XtJGAYddKle4Y6CBkW7A5DhKOnniQaSMBDjLsvxHbu/OE/Vtjqc+0Rq0rUKFlGXlFJGYtinRY85HeRLnHoarNaeAgkc5Z78TvsyOx9I12HlFrCOHrkY7Ux+XOuKoFQ+Y6HekJ4Q7r83LOkdjMxltaZFGj3erCeX6lNdp3qcZWwbG+FeFMDFGvxudtOL7ldnl0pj4Vaj+1TdUCERnf93ac44CmsQLl+MNIR/qE8jHPeBvCBFUNRHh837vkZiyuyTUov1vsNGPW9agz57Z0pg6Fiu8//HrSPvN14djMBI5xXjiW+oLGVmnpOPFDKNMcwgLJC+VRrF6n4ea1tdgsB+m0Pm/alIN1rIqb0had8abWWN+3YeN9F1wDCucxuQ8TCrJC5uUgh6ZbkMZcfH/FITMC5I9Cfoym4klrNPVFXNQfoXI8BJuNpr3sNlC1Gp2z9ot0WoNIeyUudPXeEeyfVY3QmMOsN8q8DfJXzfTMAPk6OGCg92XBEdJI66WynbVO8sYx/01/r5e0UNnrHNSLMaj8uDbLzGPxCjifX4WjfT1q5knLBOt94Wj6m0jvt7CplR0aRFUJjWm3DsEx3luVeQSRhyf0vUtNth/DdpBxJ++Dg/2TaYML83upjKrRENhOlAtWscX3X6J1PVoqjH1X3G7tUSGj1pNVnVj6vshhqYZ/2u92ENichzSusytgNH1JS+fM99s9EvQQ/5G6ChC1LpNlb03NAWRrEf7l0Nfg5SBI/0BUyNU4hmfc+qGu6a+FfCKO6WkcwwtoAC5v7bQ+Xe5lrGMh+7HTxtqgrbUvbbF0Arp25ZMWPxTq38MW2Mv46a8YsmYOMgq945nmucIJbpDyCn0svW/oiC/tJa08jukiOa6qTtS6VspV03DQ0ml9EvL7kabjJrOUj8ilJ5G0kM8TKK8B2dqEHuOj+N0HO4fTwOZPoeiM19gJby9wcIEdRG+S/cLU1xMP9PrMcPvMhNmaR6Kp7/sNceSmpDhGVVcKEM6j4nrcDhK1npa8UOAJ1XAzCvlfYNqgLL6nsnpQ2ewLF7X+btp49iAVpILC8Ux9/D4Dx/Ui0vll62Gz/kM1HUCWM23w+yYVeTEKaf5e075f4xygYtnn2cxBZGhr5LtRdieoqI628dZ4pAsn12OMWbeHGvX0qNjQuaeaftT6R7gj9WXb1s/BUDeg59i5gAblNJVuH9wOEu7se3clyB1y6eLtbScx62az1ZCAE3ss6Lby8ISZB0uhGLZ3+LXYFcaMnX4AnOSpWp7WQ6Ejjt9LxU5cDiIBeTj+gsADqVxLnTapz6rMExzDxaZ+QwcBLeNO+ICpXw7WGpkXqEo9di/n6H2eq/YKLsrDoWq6t2q0k3Ll/FsjB2ltx1ywlp+UXU5FvkBnosNGdhY0APqzTH3bpuz8vvOL+h0S1nIVbR9QqA4HCRasNZhwnjOc7g6tZslMY3T7zHEqaki4c6ZjvoOx9oCKnLgdBEMrmeuo1JfwhFkHQ786NkZF+jPS8h0uorFwXORmDhLCkMrUlxCJzviISn3BsMsx1GqLzvY8F8zljq6mi8YkhPxU5CAS7ZuLyjioP51g+ARZbUiL774NkQs4suM4GzWYaP2PNHURng11nrC/in3BeRlDemv7/m1EnYOUVyrQFdZPoFFwQ+HO1H+paWDqWgFM2FTUHFw86D9v2L4Q8qoE9UMsz+GGFzjfn5m2rR2W7/8HouJ+0tRt6iDleUBVH8f1gEoaguvwVdNOegoVOWiLWkeZenIuobGzX6/iQMDuTDMNnP/FKmpKpDM9ybS18/ehPM8y8olZ31VRQ6BX2/+HIb1Gbx+QueccZO+u9L+h1a+bMKKCdKlKYFApBs108PtrKgoEjuMW0741OrN+W4vbQTC5VElTcF5nOWyjaV/bts6+aaZuUwcBMheqph1L/0ijG4Jjmm7mI0vLKnIQGZd6B+SOlTWU76NBV7+ESCy1yrSXpVgVNWfSyRFUWnv1zA7ouTF8PEClDuRZJDMfDGdPVlFDwtHUd0w7jd4++DmIjUySYumiKUfhr0c4UDUCgcr6czMNufgqCoTHZLq+5ambg1iXq6QpOB/XWNp6QkV1hKMzJ5u6wRzE+mdFP+hSq7tnkNZXRXUgzR84dDXAGa+Rhk7VvLE3nzodTO6ZqDQQyMfhYLIqqSIHbgdBwxFo/x6ux/mmnUZvHxo6iNB1ioyhHzF1cMHvR4XcVzWaghP8o2mPYdqwnsJDi+YcAsSsoopquBwkaEUUWtqt95m2divos4CwpQ6CynShRjfEnU8jB5EJP9K906GvQRo0DJk+qZp1yPK526bhAoIHuB5LTHu0+F9UkQO3gwQpO2HndhDQEuv7APSMeQBCNJ1vtgpVASdorEThonXMOFxFgaibqMfS9ePcLXCQyPjpbzdtJYR8Jrtb7iBWoP8BGZaDCO2ntrVG+5xLykZAmV0den/9xFvuD7l1g17XCnDCmxxpdKS/oiIHu6yDCNIdmnoSZH1dxQ3BCT5o2qFgpqgoEKhg55j2uNj1c4QtcJD6rTPWBr9l1Z3WQRRZKUL5OHrsSsD1WunevjKma/oBbr2QT+PgB85vuWmPc+xTkYNd2kEEqXSmrgR7r08TMIdwLPHKfRUVBQIXwNEyypxERTXcDhKzvqWSpsjSpGmLC/JHFdWxszuITXTGa5DnRQj121ii6W+oVhlZZTPuT0mQe2AqDQTSfNi0lzmdihzs8g5SfkFC+i5TH4X7cmtnX0w1PJHVCpfNFSoKBArcteXAqr+P4HIQtKKBVouEcCz9BdMWF9z3RQw7nYPgvO3ggbxYA/nJ/rFaOrJMOu64fVTFBjqLnDrDePjt8Nmvh405yX8u5HNHfddwkCY7aNva+96MyveEaYMTeKLRyofYyLClqh+1Hgs8fxnfJ/OD6gWA7aOeti4HkVZNJU1BpVlu2oY7rG4V1bHFDhJNOVtwH4I6CM6zH2Vynf6sR3byRlPOZfIOK6pSG5nEm3I0goEffpMRhNPWf1f3VnOQ4ezy3lKksjoyd7UuXsBGXrPj2LyHuN80Wv2A/OumfrgjYOGgQpl2vq2b20EQZHVKpb7YqziO+xTWrxDtu/WhTd4TZuQRZIhpOkjQuVG4Y0a3Ix+vXhOIg0D+nGwl16g66lbpXA4iDQ4aveq+N6T5Ij7fqlJ/xM7ebFqxs16SBk2ldbgdBPk03EVdoc5BfHrMrU9vb9isHBKCVCoBhXGGaWeHqLU05LdhzbVcjJP+4z4NLqowutMab/Y8SD+PaO/K6+Eg0B+CpOFzBLjAtRUYjNlR4RuusEHfsdVENtypyBu5kWbo43yuUklDMGz9lGmHBsVzYUMdRMozo1F1yE7palqyhN1x4r+rqMroCbPGQla94RfpSC32W6ioAGc/tZougt/ybgV5dsbUR+PnuxnSBPNJx/63INtTtgqyl9/MWEKjgnYxCq3OjW57FPIKv63p4fGzDkb69jMKEmQ+47vHqGPGBMjNLe+rGq7POxyktiSN/Ob7DOdGobLPqdkgdKQ8lydNYLPAYeN1T8YADc4hpj7OI9CjyZFY6jzTDvmcpSIHFQeRhkR2G2u0Azhx7e5/1GdDI0Bax4sDVXU7rMtD3g3eKHn9qqkbpGfEuc+upo2AaxNoPoZz+4lptzm7OQIjKxTlVRvrXGRW97yC3aNgEi3bxOUFc/aKht/mvXEn74N0nJNAOw3rZZz8DfK0oLvLlS3rqPirK7r4/kw4mr5ExsGofBNhNwNhkVn4iPd93qCKOgh017d2nPQ2XMDrDft7MQ4/LhydcZDsJZPhHdKXJ9Yqx7yxYU8QnfGalo4T3yurZ6Jr2Nm2uPADSPO/5XkJtQjJ97bYiTJJdu0iQF7S8tpzNo8WOp4eA3kSx+zYVo/ff7WfrOuctZ9q2tQcRIL1gt2KV3X695Cbsijjx+00MBSTa1qWeYNGczL0q9vYkf4fWjvT/Ui3R+ZmGGJ+QRqrmtz6F+K/pObeyJOEmFPB7i9Vu3J4JhxLTfXq0WwOn/16lN/noOcoc5TFytEYrjWtE5uDmVHQIDfS1LwO2RIPnefcNrXgMaTAsAPd5ikoMPvC+QUUxC8QPJcM61AHkQupMaNwcY9B/mvc6ZoB6d89uslrVJGGYyHDL8ieJtvAfpTYW8cZnGWD3879V37BmLCLg4gTyt4rVFa931R+AAm/a3ukYtaDbdH0h9WsIeVnceyHol6o2bsCHAOOtFDuIamZJzIR97R3BffSv5SNl547qPrWQwp3uCHU6EEYIK2nl50Eu2X1A0MfmWegt5Jnsr+BnuRKOM086b4jXf6TPU+QVhvS0l8mo6S1QYGfhcr0Hbmo8gkHPS3oGyElXa9zcwcZTtkG6HG95O7gLpsxY086wEvPHXA9qjfypIeubevv30OGH+I06IUuw+cVON+s3ch4DzMbg0ZHehQ7vZj1bfTy30UvNoCyPNbdk/lhv3fA4xzcwf1oQqM6ZQZVJ4QQQgghhBBCCCGEEEIIIbsC8WxheTJbXN00DOZ9n7X2ZtOoRK6wrJZG6RwVDItjc4X9HMdRDYXfJLL5WxLZ4pXJwdK09GX3BnodawUc2/WSTiJb+FUis/gNGu0L8htfyTuRKZ6t0U3p7+/fA3l9NpkrLkF4HOlsRJ4bkM7vErniNclcoVvKStXJzgYu1lpcuE3NQk+2MKwnE5HuJNMeFfmZ5Lybh/2u12mZ0v5mOr4hW1g9ZbBwsJo1BRVzVcU2kS01fFt+b+/CPXE+99T0C4G20R9xwdV74bhuq9j5hXiueJSakJ0N00Hw/be4oCu8wpRcoeGbxd2gAi4zK0E5/Xzda/ibUe8ghZXlYyregRZ4nSmT3z39iwI94206CNLbGB8c8n3ncWKgkHbkE9BB0CjkDJsNCDegt8ugN/0ajvUnOIZ/4TzWT5u/YFh/vUC2I6aDdGfzkzV6i+jO5N8rla5cMYr3SeXQSrJWWmNVC4TbQeS3imxQ0T6KPJ6r6mSLDZ+TqOBwkLLdr3sX1h9bd3mIt97UxXkEchDk8XDVBsMsja4ydWDJG3Hsw3otEwnA1MHSW6Q1QuH+Xj575i71fHN5EKTSVi7i1nIQpHllJc1kpjQNFWyB+VvVAtHMQYRELn9RVQd5aXRD6hzEti18XsVVEPctt15wB8GcQ23imaHA/+kSlMn9hQOQ9nVSD3CcQ91z8tv/Pz12RlAojouL1ukeFQ2bre0g9kXLFl7SNP8sk+d4ptRVyQOyu1Q1EEEcpCdXHKjqZPLyJGRTTAdBBXul/Fl4xpywJ3OlD9VkxRdq+oF7kGLFBmEDersbkrn8jJ7ckuHtnvYBZemY3+AYH/LqBXcrMNF9k1ko1YB4VRkWpoPg+x34XOgOiUxhpqo3BfOM82Cj6eXPK8faK1r3V+KnZJd0lOOb08xBZOLvGMpki4GenHM4iNED1Sbsm0ZBZ6UdD4dHunNrOgHnIIOL3+MenhlprEG682SYperD4vgLlskCgD2MdYRMaVivEdrlmHbqgjZcuBedBVN4cdJFpYiqDAvTQfxC0Aohk00cy5Nqt6F7IP9mFYnjfM5IM/CbPNwOIg6AitFvh1zpEjjeYzVZYcOR2WKgN+KbDtIzp/g+2N5t/9YJe3xe4dPVdNFDwXGmV38HLA8Bab1Vj7NSLq5QeCqeLY1V9cDI8jGc7+9mWiibV6ZkbxnW2+Z3SRKZ0iwpjEqhJDPF2SoaNqaD4PujKHTX/Ybi6ni2EOgeBo7DqqYlK0yVioyACjK/lk/xlcTcZY3/5k1xO4hfQPovJ7P549SsKaaDSKsbH1hySK1MC7+F3J4/4Ps6Wa7dXAepIBW6O7f0A7KSB/tbK2nZ6eWK96nasEBackz2Aog4djJTaPqc/25Dct6Sg3ARe4O2mH7gYm2lOQiGUdnaMKpZgG6gd1YFcRBUsFdRQbyecPTF7SB2XKZwiZmuhMo9ii11EDeybF7NR3qt/qHNeuZ7ytzSgbLwgXlYoDfkkGGytRwkPlg80rjgMmZ/witUdXKFZ4Pcs/Cfg2COkC0MVWUBV68qeDmIHE+l55CQyORvsZXB5jhIPDd0FM75XK/zTA4UP1xJT8pruMvfZDuxtRwEF3l5JR2Zb2h0HZjw/7yilxwonK7Rvvg7SGWZsza2xzAr8Gs8vRxEkGGanRbmM/HM0upz7JvjIOjZ7lWb5/G9iN5oEOWEIWfxKoTq/AFlco2akJ2NreEgR2KSiwtfvjGYK/5jUn/J942RmM/UJr8Y3zfbQ9XIQQRMgKca6T0zeW4h0B/S+DlIuWcqnoCW33EDb7gOMgnHKedXzcMnQOf+qf3LvF/FQ3Y85mbFyXNv9vzrsWbAOc6ppIFxfFajPenqX9EKJ7q7oh/PDDV8u6J7s6L8VlGVeDZ/cUWOtL+p0Q1Bb2NvVpQg43iN9gX6PRX9oIsWMq+AE3wGNjfDEYzhZfF5lNldSOc0KQ9VJ4TI/Qv9SgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhIxAQqH/B1jlB1Z6efR4AAAAAElFTkSuQmCC";
                    await context.Response.WriteAsync($"<html><body><img src={image} />");
                    
                    var data = GetData();
                    await context.Response.WriteAsync("<p>Calls</p>");
                    await context.Response.WriteAsync(Environment.NewLine);
                    foreach (var call in data.userCallsVm)
                    {
                        await context.Response.WriteAsync($"<p> {call.ToString()}</p>");
                        await context.Response.WriteAsync(Environment.NewLine);
                    }

                    await context.Response.WriteAsync(Environment.NewLine);
                    
                    await context.Response.WriteAsync("<p>Info</p>");
                    await context.Response.WriteAsync(Environment.NewLine);
                    foreach (var call in data.userInfoVm)
                    {
                        await context.Response.WriteAsync($"<p> {call.ToString()}</p>");
                        await context.Response.WriteAsync(Environment.NewLine);
                    }
                    
                    await context.Response.WriteAsync("</body></html>");
                });
            });
        }

        #endregion Methods
    }
}