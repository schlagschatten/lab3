﻿namespace lab3.ViewModels
{
    public class UserInfoVm
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public int CityCount { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName + " " + PhoneNumber;
        }
    }
}